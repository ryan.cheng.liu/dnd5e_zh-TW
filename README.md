# Foundry VTT D&D 5e 系统正體中文化 & 部分模組中文化

## 支援模組
- [Obsidian](https://foundryvtt.com/packages/obsidian)
- [Monk's TokenBar](https://foundryvtt.com/packages/monks-tokenbar)
- [Babele](https://foundryvtt.com/packages/babele)
- [Token Action HUD](https://foundryvtt.com/packages/token-action-hud)
- [Combat Carousel](https://foundryvtt.com/packages/combat-carousel)
- [Monk's Enhanced Journal](https://foundryvtt.com/packages/monks-enhanced-journal)
- [Midi Quality of Life Improvementsl](https://foundryvtt.com/packages/midi-qol)


## 鳴謝
**感谢其他原中文化版本譯者**
