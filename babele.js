Hooks.once('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'dnd5e_zh-TW',
            lang: 'zh-TW',
            dir: 'compendium'
        });
    }
});

